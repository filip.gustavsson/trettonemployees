# TrettonEmployees

  A simple Angular application that displays all employees of Tretton37

## Thoughts behind code design

  When doing frontend my main focus is usually to maximize readability.
  I usually achieve that by following best practices and trying to separate the concerns of each unit as much as possible, which I've tried to do here.

  I've tried to keep things reactive, with the help of observables. The way I've set it up makes it easy to, for instance, add filtering.

## Motivation for packages

  The only packages I'm using is the ones that come along with Angular CLI.
  I choose Angular mainly because it's my favorite framework at the moment. Very easy to get started and I like the structure of the framework a lot.

## Stories

 1. Responsive design - Most people surf the web via their mobile phones these days hence why it's important
 2. Use Typescript - Types are nice!
 3. Unit Testing - I've mostly ignored unit testing the components, since I have had bad experience with Angulars testing fixture framework in previous projects.
    Instead, we usually tried to split the UI/Business logic and only write tests for the BusinessLogic. I could have added some more tests for some components, by
    manually calling the functions the UI part otherwise calls, but I've ran out of time.
 4. CI/CD Pipeline - I've setup a pipeline so that it build the project and runs all the unit tests.
 5. Sorting - Feels like a useful feature and it sounded fun to implement!

## Installing packages
Run `npm install` to install all packages

## Api Key
Update API_KEY property in `src/environments/environment.ts` with a valid api key

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding
Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
