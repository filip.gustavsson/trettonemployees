import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';

export interface EmployeeResponse {
  name: string;
  office?: string;
  imagePortraitUrl?: string;
  linkedIn?: string; // /in/username
  twitter?: string; // Only username
  gitHub?: string; // Only username
}

@Injectable()
export class HttpService {

  constructor(private _httpClient: HttpClient) { }

  public getEmployees() {
    const { API_URL, API_KEY } = environment;

    return this._httpClient.get<EmployeeResponse[]>(API_URL, {
      headers: {
        'Authorization': API_KEY,
      }
    });
  }
}
