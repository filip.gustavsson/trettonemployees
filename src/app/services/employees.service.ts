import { Injectable } from '@angular/core';
import { map, firstValueFrom } from 'rxjs';
import { EmployeeResponse, HttpService } from './http.service';

export interface Employee {
  name: string;
  office?: string;
  imageUrl?: string;
  twitterUrl?: string;
  linkedInUrl?: string;
  gitHubUrl?: string;
}

@Injectable()
export class EmployeesService {

  constructor(private _httpService: HttpService) {
  }

  public getEmployees(): Promise<Employee[]> {
    const obs$ = this._httpService.getEmployees().pipe(
      map(response => response.map(this.toView))
    );
    
    return firstValueFrom(obs$);
  }
  
  private toView(response: EmployeeResponse): Employee {
    return {
      name: response.name,
      office: response.office,
      imageUrl: response.imagePortraitUrl || `assets/person-placeholder.png`,
      twitterUrl: response.twitter && `https://twitter.com/${response.twitter}`,
      linkedInUrl: response.linkedIn && `https://www.linkedin.com${response.linkedIn}`,
      gitHubUrl: response.gitHub && `https://www.github.com/${response.gitHub}`,
    };
  }
}
