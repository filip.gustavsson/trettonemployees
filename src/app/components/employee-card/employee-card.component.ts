import { Component, Input } from '@angular/core';
import { Employee } from 'src/app/services/employees.service';

@Component({
  selector: 'app-employee-card[employee]',
  templateUrl: './employee-card.component.html',
  styleUrls: ['./employee-card.component.scss']
})
export class EmployeeCardComponent {
  @Input() public employee!: Employee;
}
