
import { of } from 'rxjs';
import { EmployeeSorter } from '../../employee-sorter/employee-sorter';
import { EmployeesContainerComponent } from './employees-container.component';

describe('EmployeesContainerComponent', () => {
  let component: EmployeesContainerComponent;

  let employeeService: any = {
    getEmployees: () => of([]),
  };

  beforeEach(() => {
    component = new EmployeesContainerComponent(employeeService, new EmployeeSorter());
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
