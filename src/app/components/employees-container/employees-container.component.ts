import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject, combineLatest, map, Observable, startWith } from 'rxjs';
import { Employee, EmployeesService } from 'src/app/services/employees.service';
import { EmployeeSorter } from '../../employee-sorter/employee-sorter';
import { SortDirection, SortingDetails, SortOption } from '../employee-sorting/employee-sorting.component';

@Component({
  selector: 'app-employees-container',
  templateUrl: './employees-container.component.html',
  styleUrls: ['./employees-container.component.scss']
})
export class EmployeesContainerComponent {

  public loading = false;
  public error?: string;

  public sortingControl = new FormControl<SortingDetails>({
    sortOption: SortOption.NAME,
    sortDirection: SortDirection.Descending,
  });

  public employees$: Observable<Employee[]>;

  private employeesSubject = new BehaviorSubject<Employee[]>([]);

  constructor(private _service: EmployeesService, private _employeeSorter: EmployeeSorter) {
    this.getEmployees();

    this.employees$ = combineLatest([
      this.employeesSubject,
      this.sortingControl.valueChanges.pipe(startWith(this.sortingControl.value))
    ]).pipe(map(([employees, sortingDetails]) => {
      if (employees && sortingDetails) {
        return this._employeeSorter.sort(employees, sortingDetails);
      }

      return [];
    }));
  }

  private async getEmployees() {
    this.loading = true;
    this.error = undefined;

    try {
      const employees = await this._service.getEmployees();
      this.employeesSubject.next(employees);
    } catch(e) {
      this.error = 'Something went wrong while fetching the employees :(';
    } finally {
      this.loading = false;
    }
  }
}
