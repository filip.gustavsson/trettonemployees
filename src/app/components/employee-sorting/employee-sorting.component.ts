import { Component, OnDestroy, OnInit, Optional, Self } from '@angular/core';
import { ControlValueAccessor, FormControl, NgControl } from '@angular/forms';
import { combineLatest, startWith, Subscription } from 'rxjs';

export enum SortOption {
  NO_SORTING = "No sorting",
  NAME = "Name",
  OFFICE = "Office",
}

export enum SortDirection {
  Descending = "Descending",
  Ascending = "Ascending",
}

export interface SortingDetails {
  sortOption: SortOption;
  sortDirection: SortDirection;
}

@Component({
  selector: 'app-employee-sorting',
  templateUrl: './employee-sorting.component.html',
  styleUrls: ['./employee-sorting.component.scss']
})
export class EmployeeSortingComponent implements OnInit, OnDestroy, ControlValueAccessor {

  public readonly sortOptions = [
    SortOption.NO_SORTING,
    SortOption.NAME,
    SortOption.OFFICE,
  ];

  public readonly sortDirections = [
    SortDirection.Descending,
    SortDirection.Ascending,
  ];

  public sortOptionControl = new FormControl<SortOption>(SortOption.NO_SORTING);
  public sortDirectionControl = new FormControl<SortDirection>(SortDirection.Descending);

  private sub?: Subscription;

  // ControlValueAccessor interface
  private _onChange = (value: SortingDetails) => undefined;
  private _onTouched = () => undefined;

  public ngOnInit() {
    this.sub = combineLatest([
      this.sortOptionControl.valueChanges.pipe(startWith(this.sortOptionControl.value)),
      this.sortDirectionControl.valueChanges.pipe(startWith(this.sortDirectionControl.value)),
    ]).subscribe(([sortOption, sortDirection]) => {

      if (sortOption && sortDirection) {
        this._onChange({sortOption, sortDirection});
        this._onTouched();
      }
    });
  }

  public ngOnDestroy() {
    this.sub?.unsubscribe();
  }

  // This is how we get access to the outer [formControl]
  constructor(@Self() @Optional() private control: NgControl) {
    this.control && (this.control.valueAccessor = this);
  }

  public writeValue(value: SortingDetails) {
    this.sortOptionControl.setValue(value.sortOption, {emitEvent: false});
    this.sortDirectionControl.setValue(value.sortDirection, {emitEvent: false});
  }

  public registerOnChange(fn: any) {
    this._onChange = fn;
  }

  public registerOnTouched(fn: any) {
    this._onTouched = fn;
  }

  public setDisabledState?(isDisabled: boolean) {
    if (isDisabled) {
      this.sortOptionControl.disable();
      this.sortDirectionControl.disable();
    } else {
      this.sortOptionControl.enable();
      this.sortDirectionControl.enable();
    }
  }
}
