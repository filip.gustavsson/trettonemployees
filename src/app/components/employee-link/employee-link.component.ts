import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-employee-link[href]',
  templateUrl: './employee-link.component.html',
  styleUrls: ['./employee-link.component.scss']
})
export class EmployeeLinkComponent {
  @Input() href!: string;
}
