import { Component, Input } from '@angular/core';
import { Employee } from 'src/app/services/employees.service';

@Component({
  selector: 'app-employee-cards-container[employees]',
  templateUrl: './employee-cards-container.component.html',
  styleUrls: ['./employee-cards-container.component.scss']
})
export class EmployeeCardsContainerComponent {
  @Input() employees: Employee[] = [];
}
