import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { EmployeesService } from './services/employees.service';
import { HttpService } from './services/http.service';
import { EmployeesContainerComponent } from './components/employees-container/employees-container.component';
import { EmployeeCardComponent } from './components/employee-card/employee-card.component';
import { EmployeeLinkComponent } from './components/employee-link/employee-link.component';
import { EmployeeCardsContainerComponent } from './components/employee-cards-container/employee-cards-container.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EmployeeSortingComponent } from './components/employee-sorting/employee-sorting.component';
import { EmployeeSorter } from './employee-sorter/employee-sorter';

@NgModule({
  declarations: [
    AppComponent,
    EmployeesContainerComponent,
    EmployeeCardComponent,
    EmployeeLinkComponent,
    EmployeeCardsContainerComponent,
    EmployeeSortingComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [
    EmployeesService,
    HttpService,
    EmployeeSorter,
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }
