import { SortDirection, SortOption } from "../components/employee-sorting/employee-sorting.component";
import { Employee } from "../services/employees.service";
import { EmployeeSorter } from "./employee-sorter";

function mockEmployees(): Employee[] {

  const templateEmployee: Employee = {
    gitHubUrl: '',
    imageUrl: '',
    linkedInUrl: '',
    twitterUrl: '',
    name: '',
    office: '',
  };

  return [
    {
      ...templateEmployee,
      name: 'Calle Kula',
      office: 'Calcutta'
    },
    {
      ...templateEmployee,
      name: 'Adolf Gren',
      office: undefined,
    },
    {
      ...templateEmployee,
      name: 'Bertil Jönsson',
      office: 'Berts hus'
    },
  ];
}

describe('EmployeeSorter', () => {

  const sorter = new EmployeeSorter();
  const employees = mockEmployees();

  it('should create', () => {
    expect(sorter).toBeTruthy();
  });

  it('should NOT sort', () => {
    const sorted = sorter.sort(employees, {sortOption: SortOption.NO_SORTING, sortDirection: SortDirection.Descending});
    expect(sorted.length).toBe(3);
    expect(sorted[0].name).toBe('Calle Kula');
    expect(sorted[1].name).toBe('Adolf Gren');
    expect(sorted[2].name).toBe('Bertil Jönsson');
  });

  it('should sort on name DESCENDING', () => {
    const sorted = sorter.sort(employees, {sortOption: SortOption.NAME, sortDirection: SortDirection.Descending});
    expect(sorted[0].name).toBe('Calle Kula');
    expect(sorted[1].name).toBe('Bertil Jönsson');
    expect(sorted[2].name).toBe('Adolf Gren');
  });

  it('should sort on name ASCENDING', () => {
    const sorted = sorter.sort(employees, {sortOption: SortOption.NAME, sortDirection: SortDirection.Ascending});
    expect(sorted[0].name).toBe('Adolf Gren');
    expect(sorted[1].name).toBe('Bertil Jönsson');
    expect(sorted[2].name).toBe('Calle Kula');
  });

  it('should sort on office DESCENDING', () => {
    const sorted = sorter.sort(employees, {sortOption: SortOption.OFFICE, sortDirection: SortDirection.Descending});
    expect(sorted[0].office).toBe('Calcutta');
    expect(sorted[1].office).toBe('Berts hus');
    expect(sorted[2].office).toBeUndefined();
  });

  it('should sort on office ASCENDING', () => {
    const sorted = sorter.sort(employees, {sortOption: SortOption.OFFICE, sortDirection: SortDirection.Ascending});
    expect(sorted[0].office).toBeUndefined();
    expect(sorted[1].office).toBe('Berts hus');
    expect(sorted[2].office).toBe('Calcutta');
  });
});
