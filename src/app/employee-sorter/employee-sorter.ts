import { Injectable } from "@angular/core";
import { SortDirection, SortingDetails, SortOption } from "../components/employee-sorting/employee-sorting.component";
import { Employee } from "../services/employees.service";

@Injectable()
export class EmployeeSorter {

  public sort(employees: Employee[], sortingDetails: SortingDetails) {
    const copyEmployees = [...employees];

    if (sortingDetails.sortOption === SortOption.NAME) {
      return this.sortByName(copyEmployees, sortingDetails.sortDirection);
    }

    if (sortingDetails.sortOption === SortOption.OFFICE) {
      return this.sortByOffice(copyEmployees, sortingDetails.sortDirection);
    }

    return copyEmployees;
  }

  private sortByName(employees: Employee[], direction: SortDirection) {
    return employees.sort((a, b) => {
      if (direction === SortDirection.Descending) {
        return b.name.localeCompare(a.name);
      }
      return a.name.localeCompare(b.name);
    });
  }

  private sortByOffice(employees: Employee[], direction: SortDirection) {
    if (direction === SortDirection.Descending) {
      return this.sortByOfficeDescending(employees);
    }

    if (direction === SortDirection.Ascending) {
      return this.sortByOfficeAscending(employees);
    }

    return employees;
  }

  private sortByOfficeDescending(employees: Employee[]) {
    return employees.sort((a, b) => {

      if (!a.office && !b.office) {
        return 0;
      }

      if (!a.office) {
        return 1;
      }

      if (!b.office) {
        return -1;
      }

      return b.office.localeCompare(a.office);
    });
  }

  private sortByOfficeAscending(employees: Employee[]) {
    return employees.sort((a, b) => {

      if (!a.office && !b.office) {
        return 0;
      }

      if (!a.office) {
        return -1;
      }

      if (!b.office) {
        return 1;
      }

      return a.office.localeCompare(b.office);
    });
  }
}
